#include <stdio.h>
#include <stdlib.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h> 

#include <sys/errno.h>

/*
	 int mkfifo( char * path, mode_t mode ); 

	 int open ( char *path, int flags); 

	flags:
		O_RDONLY – tryb tylko do odczytu
		O_WRONLY– tryb tylko do zapisu 
*/

#define true 1

int main(int argc, char *argv[]){
	int fd;

	fd = open("./fifka",O_RDONLY);
	if(fd == -1){
		perror("[C] open error");
		exit(EXIT_FAILURE);
	}

	int respond;
	char buf;

	pid_t pid = getpid();

	FILE* file;
	char filename[16] = "client_";
	sprintf(&filename[7], "%d", pid);
	file = fopen(filename,"w");
	if(file == NULL) {
	    perror("fopen error");
	    exit(EXIT_FAILURE);
	}


	while(true){
		respond = read(fd,(char*)&buf,sizeof(buf));
		if(respond == -1){
			perror("read error");
			exit(EXIT_FAILURE);
		} else if (respond == 0){
			break;
		}
		putc(buf, file);
		usleep(50);
		printf("[C - %d] read : %c\n",getpid(),buf);
	}

	if(close(fd) == -1){
		perror("close error");
		exit(EXIT_FAILURE);
	}

	fclose(file);

	return 0;
}