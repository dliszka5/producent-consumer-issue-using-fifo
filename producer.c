#include <stdio.h>
#include <stdlib.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h> 

#include <sys/errno.h>

/*
	 int mkfifo( char * path, mode_t mode ); 

	 int open ( char *path, int flags); 

	flags:
		O_RDONLY – tryb tylko do odczytu
		O_WRONLY– tryb tylko do zapisu 
*/


#define data_size 100

int main(int argc, char *argv[]){

	int fd;
	fd = open("./fifka",O_WRONLY | O_NONBLOCK);
	if(fd == -1){
		perror("[P] open error");
		exit(EXIT_FAILURE);
	}

	char buf;
	int respond;

	pid_t pid = getpid();

	FILE* file;
	char filename[16] = "server_";
	sprintf(&filename[7], "%d", pid);
	file = fopen(filename,"w");
	if(file == NULL) {
	    perror("fopen error");
	    exit(EXIT_FAILURE);
	}


	for(int i=0; i<data_size; ++i){
		//buf = (char) (rand()%96 + 32);
		buf = (char) ('a' + rand()%26);
		respond = write(fd,(char*)&buf,sizeof(buf));
		if(respond == -1){
			perror("write error");
			exit(EXIT_FAILURE);
		} else {
			putc(buf, file);
		}
		printf("[P - %d] write : %c\n",getpid(),buf);
	}

	if(close(fd) == -1){
		perror("close error");
		exit(EXIT_FAILURE);
	}

	fclose(file);
	
	return 0;
}