#include <stdio.h>
#include <stdlib.h>

#include <sys/unistd.h>

#include <sys/types.h>
#include <sys/stat.h> 

#include <sys/wait.h>

#include <sys/errno.h>

#include  <signal.h>

/*
	 int mkfifo( char * path, mode_t mode ); 

	 int open ( char *path, int flags); 

	flags:
		O_RDONLY – tryb tylko do odczytu
		O_WRONLY– tryb tylko do zapisu 
*/

void interrupt_handler(int signal);

int main(int argc, char *argv[]){
	
	int numberOfCustomers = 0;
	int numberOfProducers = 0;

	pid_t customer;
	pid_t producer;

	int status;

	pid_t pid;

	if(argc != 3){
		numberOfProducers = 1;
		numberOfCustomers = 1;
	} else {
    	numberOfProducers = atoi(argv[1]) > 0 ? atoi(argv[1]) : -atoi(argv[1]);
    	numberOfCustomers = atoi(argv[2]) > 0 ? atoi(argv[2]) : -atoi(argv[2]);
	}

  if(numberOfCustomers + numberOfCustomers > 200){
    printf("Too many processes!\n");
    printf("Using default numberOfCustomers & numberOfProducers instead\n");

    numberOfProducers = 100;
    numberOfCustomers = 100;

  }

	if(mkfifo("fifka",0600) == -1){
		perror("mkfifo error");
		exit(EXIT_FAILURE);
	}

	signal(SIGINT,interrupt_handler);

	system("rm server*");
	system("rm client*");

	for(int i = 0; i < numberOfCustomers; ++i){
		switch(customer = fork()){
			case -1		:	perror("fork error");
							exit(EXIT_FAILURE);
							break;
			case  0		:	if(execl("./customer","customer",NULL) == -1){
	                      		perror("execl error");
	                      		exit(EXIT_FAILURE);
	                    	}
							break;
			default 	:	printf("Customer created : %d\n",customer);
							break;
		}
	}

	for(int i = 0; i < numberOfProducers; ++i){
		switch(producer = fork()){
			case -1		:	perror("fork error");
							exit(EXIT_FAILURE);
							break;
			case  0		:	if(execl("./producer","producer",NULL) == -1){
	                      		perror("execl error");
	                      		exit(EXIT_FAILURE);
	                    	}
							break;
			default 	:	printf("Producer created : %d\n",producer);
							break;
		}
	}
	
	for(int i = 0; i < numberOfCustomers+numberOfProducers; ++i){
		pid = wait(&status);
		if(pid == -1){
			perror("wait error");
			exit(EXIT_FAILURE);
		}
		printf("%d terminated\n",pid);
	}
	printf("..:: All proces terminated! ::..\n");

	if(unlink("fifka") == -1){
		perror("unlink error");
		exit(EXIT_FAILURE);
	}

	return 0;
}

void interrupt_handler(int signal){	
	if (signal == SIGINT) {
		if(unlink("fifka") == -1){
			perror("unlink error");
			exit(EXIT_FAILURE);
		}
	} else {
		exit(0);
	}
}